var app = angular.module('directiveApp', []);

app.directive('editable', function () {
    return {
        restrict: 'A',
        transclude: true,
        templateUrl: 'editUserInfo.html',
        scope: true
    }
});
